from core.document_tokenizer import tokenize_string

AMOUNT_OF_LINES_IN_SNIPPET = 4


class AnswerProcessor:
    def __init__(self, answer, query):
        self._answer = answer
        self._tokenize_query = tokenize_string(query)

    def _get_snippet_start_index(self, tokenized_content):
        maximum_amount_of_the_same_tokens = 0
        max_index = 0
        for index in range(0, len(tokenized_content) - len(self._tokenize_query) + 1):
            count = 0
            for j in range(0, len(self._tokenize_query)):
                if tokenized_content[index + j] == self._tokenize_query[j]:
                    count += 1
            if maximum_amount_of_the_same_tokens < count:
                maximum_amount_of_the_same_tokens = count
                max_index = index
        return max_index

    def _get_snippet(self, snippet_start_index, tokens):
        start_index = snippet_start_index
        while tokens[start_index] != "\n" and start_index != 0:
            start_index -= 1
        if start_index != 0:
            start_index += 1
        end_index = snippet_start_index - 1
        for line in range(AMOUNT_OF_LINES_IN_SNIPPET):
            end_index += 1
            while end_index != len(tokens) and tokens[end_index] != "\n":
                end_index += 1
            if end_index == len(tokens):
                break
        return " ".join(tokens[start_index: end_index])

    def build_answer(self):
        result = []
        for url, content in self._answer:
            print(url)
            tokenized_content = tokenize_string(content)
            snippet_start_index = self._get_snippet_start_index(tokenized_content)
            snippet = self._get_snippet(snippet_start_index, tokenized_content)
            result.append([url, snippet])
        return result
